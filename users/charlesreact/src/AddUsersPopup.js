import React from 'react';
import AddUser from './AddUser'; 
import FilterUsers from './FilterUsers'


class AddUsersPopup extends React.Component {

    state = {
        showPopup:false
    }

    togglePopup () {
        this.setState({
            showPopup: !this.state.showPopup
        })
        this.props.allUsers ()
    }

    render() {
      return (
        <div className="tabs">
                  
                  <div> 
                    <FilterUsers selectUsers={this.props.selectUsers.bind(this)} findUsers={this.props.findUsers.bind(this)}  allUsers={this.props.allUsers.bind(this)}/> 
                     
                        
                  </div>
                  <button className="small-button" onClick={this.togglePopup.bind(this)}>+</button>
                            {this.state.showPopup ? 
                                  <div className='popup'>
                                    <div className='popup-inner'>
                                          <h1>Add new item</h1>
                                                    
                                                    <AddUser togglePopup={this.togglePopup.bind(this)}/> 
                                                    <div className="button-under">  
                                                    <button className="small-button" onClick={this.togglePopup.bind(this)}>Back</button>
                                                    </div>
                                    </div>  
                                  </div>
                                  
                              : null
                            }
             
                        <button className="small-button" onClick={this.props.allUsers}>All</button>
                 
                
        </div>
      );
    }
  };
  
export default AddUsersPopup
