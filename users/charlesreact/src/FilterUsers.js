import React from 'react';
import './App.css';
import axios from 'axios'; 

class FilterUsers extends React.Component{

    state= {
        inputText:'', 
        users : []
    }

    inputChanged (event) {
        const newInputText=event.target.value
        this.setState({
            inputText: newInputText
        })
    }
    searchUsers = async event => {
        event.preventDefault();
        const productName = this.state.inputText
        console.log(productName)
        if (productName === "" || productName === null || productName === undefined ){
            this.props.allUsers()
        } 
        else {
            try{
              const response = await axios.get(`${'http://localhost:3001/routes/users/'+productName}`)
              console.log(response.data)   
              if (response.data === "") {
                  this.props.allUsers()
              } else {
                this.props.findUsers (response)
              }
            }

            catch( error ){
              console.log("error")
              this.props.allUsers()
            }
           
           }
    }

    filterAccountAdmin = async users => {
            let url='http://localhost:3001/routes/users/';
            this.setState ({
            }) 
            try {

                this.setState({
                    users:[]
                })
                  const res = await axios.get(url);
                
                let copyUsers = [...this.state.users]; 
          
                  for(var i=0; i<res.data.length; i++) {
                    copyUsers.push(res.data[i])
          
                  }

                  copyUsers.map(user => {

                    const users = [...this.state.users]
                        if (user.administrator === true ){ 
                      users.push(user)}
                      this.setState ({
                        users
                    })
                  }) 
                    console.log(this.state.users)
                   this.props.selectUsers(this.state.users)

                }
            
               
            catch(error){
                this.setState({error:'something went wrong'})
                }
          
        }
          
        filterAccountClients = async users => {
            let url='http://localhost:3001/routes/users/';
            this.setState ({
            }) 
            try {

                this.setState({
                    users:[]
                })
                  const res = await axios.get(url);
                
                let copyUsers = [...this.state.users]; 
          
                  for(var i=0; i<res.data.length; i++) {
                    copyUsers.push(res.data[i])
          
                  }

                  copyUsers.map(user => {

                    const users = [...this.state.users]
                        if (user.administrator === false ){ 
                      users.push(user)}
                      this.setState ({
                        users
                    })
                  }) 
                    console.log(this.state.users)
                   this.props.selectUsers(this.state.users)

                }
            
               
            catch(error){
                this.setState({error:'something went wrong'})
                }
          
        }
         


    render() {
        return (
            <div>
            <div className="tabs-filter">
                <form onSubmit={this.searchUsers.bind(this)}> 
                        <input placeholder="email" onChange={this.inputChanged.bind(this)} className="tabs-input" />
                        <button type="submit" className="small-button">Search</button>
                </form> 
                        <div className="dropdown">
                            <button className="small-button">Account</button>
                            <div className="dropdown-content">
                                <a onClick={this.filterAccountAdmin}>Administrators</a>
                                <a onClick={this.filterAccountClients}>Clients</a>
                            </div>
                        </div>
            </div>
            </div>
              )
        
    }
}

export default FilterUsers