import React from 'react';
import './App.css';
import { NavLink } from "react-router-dom"; 
import  { Redirect } from 'react-router-dom'


class Header extends React.Component {

    state={
        email: '' ,
        loggedIn: false,
    }


   
    componentDidMount (props) {
        console.log('header component did mount')
        // const email = this.props.email
        // const loggedIn = this.props.loggedIn
        const email = localStorage.getItem('email'); 
        const loggedIn = localStorage.getItem('loggedIn'); 
                        this.setState ({
                            email : email, 
                            loggedIn: loggedIn
                        })
    }


    logOut () {
        localStorage.setItem('email', '');
        localStorage.setItem('loggedIn', false);
        this.setState ({
            email: '', 
            loggedIn: false, 
        })
       
    }



    render () {
        // console.log(this.props)
        // console.log('load header')
        // console.log('storage in header', localStorage)
        // console.log('state in header', this.state)
    return (
        
        <div className="header">
        
            <NavLink to="/"><h3>CHARLES</h3></NavLink>
            {           
            this.state.loggedIn === true || this.state.loggedIn ==='true'? (
                <p className="navlink">Account: {this.state.email}
                    <button onClick={this.logOut.bind(this)}>Log Out</button> </p> 
            ):  <NavLink className="navlink" to="/login">Log in</NavLink>
            }
     
        </div>
    )
            }
}

export default Header





