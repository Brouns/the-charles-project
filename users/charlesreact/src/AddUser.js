import React from 'react';
import axios from 'axios'; 

class AddUser extends React.Component {

state = {
    firstname: '',
    lastname:'',
    email:'',
    password:'',
    admin: false
}

changeFirstname (event) {
    const newName = event.target.value
    this.setState ({
        firstname: newName 
    })
}

changeLastname (event) {
    const newName = event.target.value
    this.setState ({
        lastname: newName 
    })
}

changeEmail(event) {
    const newEmail = event.target.value
    this.setState ({
        email: newEmail
    })
}

changePassword (event) {
    const newPassword = event.target.value
    this.setState ({
        password: newPassword
    })
}

changeAdmin (event) {
    const newAdmin = event.target.value
    this.setState ({
        administrator: newAdmin 
    })
}


    handleSubmit = async event => {
        console.log('handleSubmit')
            event.preventDefault();
            const { firstname, lastname, email, password, administrator } = this.state
            try{
              const response = await axios.post(`${'http://localhost:3001/routes/users/new'}`,{firstname, lastname, email, password, administrator})
              console.log(response)
            }
            catch( error ){
              console.log(error)
            }
            this.props.togglePopup()
           }
      

      render () {
          return (
            <div>
            <form onSubmit={this.handleSubmit.bind(this)}>
                                      
                <div className="popup-form"> 
                    <label for="firstname">Firstname: </label>
                    <input onChange={this.changeFirstname.bind(this)} type="text" name="firstname" />  
                    
                    <div></div>
                    <div></div>

                    <div></div>
                    <div></div>

                    <label  for="lastname">Lastname: </label>
                    <input  onChange={this.changeLastname.bind(this)} type="text" name="lastname" />

                    <label  for="email">Email: </label> 
                    <input onChange={this.changeEmail.bind(this)} type="email" name="email" />

                    <label  for="password">Password: </label> 
                    <input onChange={this.changePassword.bind(this)} type="text" name="password" />
                                                        
                    <label  for="admin">Admin: </label>
                    <input onChange={this.changeAdmin.bind(this)} type="bolean" name="admin" />

                    
                    </div>
            </form>
                    <div className="button-under">  
                    <button class="small-button" onClick={this.handleSubmit.bind(this)} >Add</button>
                    </div>
              </div>      
                    

          )}
      
          }

export default AddUser