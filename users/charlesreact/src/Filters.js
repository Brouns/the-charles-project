import React from 'react';
import './App.css';
import axios from 'axios'; 

class Filter extends React.Component{

    state= {
        inputText:'', 
        products : []
    }

    inputChanged (event) {
        const newInputText=event.target.value
        this.setState({
            inputText: newInputText
        })
    }
    searchProduct = async event => {
        event.preventDefault();
        const productName = this.state.inputText
        console.log(productName)
        if (productName === "" || productName === null || productName === undefined ){
            this.props.allProducts()
        } 
        else {
            try{
              const response = await axios.get(`${'http://localhost:3001/routes/products/'+productName}`)
              console.log(response.data)   
              if (response.data === "") {
                  this.props.allProducts()
              } else {
                this.props.findProduct (response)
              }
            }

            catch( error ){
              console.log("error")
              this.props.allProducts()
            }
           
           }
    }


    render() {
        return (
            <div>
            <div className="tabs-filter">
                <form onSubmit={this.searchProduct.bind(this)}> 
                        <input placeholder="name" onChange={this.inputChanged.bind(this)} className="tabs-input" />
                        <button type="submit" className="small-button">Search</button>
                </form>
            </div>
            </div>
              )
        
    }
}

export default Filter