import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom" 

import Administrator from './Administrator';
import Landingspage from './Components/Landingspage'; 
import About from './Components/About'; 
import Contact from './Components/Contact.js'; 
import Error from './Components/Error';
import LoginPopup from './Components/LoginPopup'; 
import NewAccount from './Components/NewAccount'
import Chart from './Components/Chart'
import Pay from './Components/Pay'
import Reviews from './Components/Reviews';
import AdminStart from './AdminStart';
import Users from './Users';



import LionelMessi from './Components/LionelMessi'; 
import MeghanMarkle from './Components/MeghanMarkle';
import JustinTrudeau from './Components/JustinTrudeau';
import MerylStreep from './Components/MerylStreep';
import DonaldGlover from './Components/DonaldGlover';
import AlexandriaOcasioCortez from './Components/Alexandria Ocasio-Cortez';





class App extends React.Component {



    render () {
        return (
          <div>
            <BrowserRouter>
            <div>
                {/* <Header />  */}
                <Switch>
                  <Route path="/" component={Landingspage} exact />
                  <Route path="/about" component={About} />
                  <Route path="/contact" component={Contact} />
                  <Route path="/products" component={Administrator} />
                  <Route path="/login" component={LoginPopup} />
                  <Route path="/LionelMessi" component={LionelMessi}/> 
                  <Route path="/MeghanMarkle" component={MeghanMarkle}/>
                  <Route path="/JustinTrudeau" component={JustinTrudeau}/> 
                  <Route path="/DonaldGlover" component={DonaldGlover}/> 
                  <Route path="/MerylStreep" component={MerylStreep}/> 
                  <Route path="/Alexandria Ocasio-Cortez" component={AlexandriaOcasioCortez}/>
                  <Route path="/newAccount" component={NewAccount}/>
                  <Route path="/chart" component={Chart}/>
                  <Route path="/pay" component={Pay}/>
                  <Route path="/reviews" component={Reviews} /> 
                  <Route path="/admin" component={AdminStart} /> 
                  <Route path="/users" component={Users} /> 
                  <Route component={Error}/>
                </Switch> 
            </div>
            </BrowserRouter>
           
          </div>
        )
    }
}

export default App

