import React from 'react';

import AddProduct from './AddProduct'; 
import Filters from './Filters'


class AddProductPopup extends React.Component {

    state = {
        showPopup:false
    }

    togglePopup () {
        this.setState({
            showPopup: !this.state.showPopup
        })
        this.props.allProducts ()
    }

    render() {
      return (
        <div className="tabs">
                  
                  <div> 
                    <Filters findProduct={this.props.findProduct.bind(this)}  allProducts={this.props.allProducts.bind(this)}/>
                     
                        
                  </div>
                  <button className="small-button" onClick={this.togglePopup.bind(this)}>+</button>
                            {this.state.showPopup ? 
                                  <div className='popup'>
                                    <div className='popup-inner'>
                                          <h1>Add new item</h1>
                                                    
                                                    <AddProduct togglePopup={this.togglePopup.bind(this)}/> 
                                                    <div className="button-under">  
                                                    <button className="small-button" onClick={this.togglePopup.bind(this)}>Back</button>
                                                    </div>
                                    </div>  
                                  </div>
                                  
                              : null
                            }
             
                        <button className="small-button" onClick={this.props.allProducts}>All</button>
                 
                
        </div>
      );
    }
  };
  
export default AddProductPopup
