import React from 'react';
import axios from 'axios'; 
import './App.css';


class UpdateProduct extends React.Component{
 
product = this.props.product

state ={
   
}
changeName (event) {
    const newName = event.target.value
    this.setState ({
        name: newName 
    })
}

changeFiveMin (event) {
    const newFiveMin = event.target.value
    this.setState ({
        fiveMin: newFiveMin 
    })
}

changeFiveMinSold (event) {
    const newFiveMinSold = event.target.value
    this.setState ({
        fiveMinSold: newFiveMinSold 
    })
}

changeFiveMinStock (event) {
    const newFiveMinStock = event.target.value
    this.setState ({
        fiveMinStock: newFiveMinStock 
    })
}




changeTenMin (event) {
    const newTenMin = event.target.value
    this.setState ({
        tenMin: newTenMin 
    })
}

changeTenMinSold(event){
    const newTenMinSold = event.target.value
    this.setState ({
        tenMinSold: newTenMinSold
    })
}

changeTenMinStock (event) {
    const newTenMinStock = event.target.value
    this.setState ({
        tenMinStock: newTenMinStock 
    })
}




changeTwentyMin (event) {
    const newTwentyMin = event.target.value
    this.setState ({
        twentyMin: newTwentyMin
    })
}

changeTwentyMinSold(event){
    const newTwentyMinSold = event.target.value
    this.setState ({
        twentyMinSold: newTwentyMinSold
    })
}

changeTwentyMinStock (event) {
    const newTwentyMinStock = event.target.value
    this.setState ({
        twentyMinStock: newTwentyMinStock 
    })
}

updateProduct  = async event => {
    console.log(this.state)
    event.preventDefault();
    const { name , fiveMin, fiveMinSold, fiveMinStock, tenMin, tenMinSold, tenMinStock, twentyMin, twentyMinSold, twentyMinStock } = this.state
    const id = this.props.product._id
    console.log(id)
    try{
      const response = await axios.post(`${'http://localhost:3001/routes/products/update'}`,{id, name, fiveMin, fiveMinSold, fiveMinStock, tenMin, tenMinSold, tenMinStock, twentyMin, twentyMinSold, twentyMinStock})
      console.log(response)
    }
    catch( error ){
      console.log(error)
    }
    this.props.togglePopup()
   }




render (){
    return (
        <div>
            <form onSubmit={this.updateProduct.bind(this)}>
        <div className="popup-form-update"> 
    
                    <label for="name"> {this.props.product.name} </label>
                    <input onChange={this.changeName.bind(this)} type="text" name="name" />
      
                    <div></div>
                    <div></div>

                    <div></div>
                    <div></div>

                    <label  for="fiveMin">5min: ${this.props.product.fiveMin} </label>
                    <input  onChange={this.changeFiveMin.bind(this)} type="number" name="fiveMin" />

                    <label  for="fiveMinSold">Sold:{this.props.product.fiveMinSold} </label> 
                    <input onChange={this.changeFiveMinSold.bind(this)} type="number" name="fiveMinSold" />

                    <label  for="fiveMinStock">In stock:{this.props.product.fiveMinStock} </label> 
                    <input onChange={this.changeFiveMinStock.bind(this)} type="number" name="fiveMinStock" />


                                                        
                    <label  for="tenMin">10min: ${this.props.product.tenMin}</label>
                    <input onChange={this.changeTenMin.bind(this)} type="number" name="tenMin" />

                    <label  for="tenMinSold">Sold:{this.product.tenMinSold} </label> 
                    <input onChange={this.changeTenMinSold.bind(this)} type="number" name="tenMinSold" />

                    <label for="tenMinStock">In stock: {this.product.tenMinStock}</label>
                    <input onChange={this.changeTenMinStock.bind(this)} type="number" name="tenMinStock" />


                                                        
                    <label  for="twentyMin">20min: ${this.product.twentyMin} </label>
                    <input onChange={this.changeTwentyMin.bind(this)} type="number" name="twentyMin"  />

                    <label  for="twentyMinSold">Sold:{this.product.twentyMinSold} </label> 
                    <input onChange={this.changeTwentyMinSold.bind(this)} type="number" name="twentyMinSold" />

                    <label for="twentyMinStock">In stock:{this.product.changeTwentyMinStock}</label>
                    <input onChange={this.changeTwentyMinStock.bind(this)} type="number" name="twentyMinStock" /> 
                
        </div>  
        </form>
                    <div className="button-under">  
                    <button className="small-button" onClick={this.updateProduct.bind(this)} >Change</button>
                    </div>
    </div>     
    
    )}

}

export default UpdateProduct