import React from 'react';
import axios from 'axios'; 
import './App.css';


import Trashcan from './logos/trashcan.png'

class DeleteProduct extends React.Component{

    deletePerson = async event => {
        const id= this.props.id
    
            try{
              const response = await axios.post(`${'http://localhost:3001/routes/products/delete'}`,{id})
              console.log(response)
            }
            catch( error ){
              console.log(error)
            }
            this.props.allProducts ()
           }


    render (){
        return (
            <button className="table-button" onClick={this.deletePerson.bind(this)}><img  src={Trashcan} height="20"/></button>
        )
    }
}




export default DeleteProduct