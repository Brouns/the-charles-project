import React from 'react';
import axios from 'axios'; 
import './App.css';

import HeaderAdmin from './HeaderAdmin'; 

import AddProductPopup from './AddProductPopup'
import DeleteProduct from './DeleteProduct'
import UpdateProductPopup from './UpdateProductPopup'



class Administrator extends React.Component{

state={
  products : []
}



componentDidMount(){
  this.allProducts()
}


allProducts = async products => {
  let url='http://localhost:3001/routes/products/';
  this.setState ({
    products:[]
  }) 
  try {
        const res = await axios.get(url);
 
        let copyProducts = [...this.state.products]; 

        for(var i=0; i<res.data.length; i++) {
          copyProducts.push(res.data[i])

        }
    this.setState({
            products : copyProducts
          }) 
      }
     
  catch(error){
      this.setState({error:'something went wrong'})
      }

  }

findProduct (props) {
  this.setState({
    products: [props.data]
  })
}

render () {

  return(
    <div>
      <HeaderAdmin email="administrator" loggedIn= {true} /> 
      <div className="headerCharles">
          <h1>CHARLES</h1>
          <h3>The Project</h3>
      </div>
      <AddProductPopup  allProducts={this.allProducts.bind(this)} findProduct={this.findProduct.bind(this)} /> 
      
            <div className="table-header">
                  <div>ID number</div>
                  <div>Name</div>
                  <div>5min</div>
                  <div>Sold</div>
                  <div>Stock</div>
                  <div>10min</div>
                  <div>Sold</div>
                  <div>Stock</div>
                  <div>20min</div>
                  <div>Sold</div>
                  <div>Stock</div>
                  <div></div>

            </div>
    { this.state.products.map((item, key) => {
      return (
            <div className="product">
                 <div className="table-products">
                            <div className="product-classification">{this.state.products[key]._id}</div>
                            <div>{this.state.products[key].name}</div>
                            <div>${this.state.products[key].fiveMin}</div>
                            <div>{this.state.products[key].fiveMinSold}</div>
                            <div>{this.state.products[key].fiveMinStock}</div>
                            
                            <div>${this.state.products[key].tenMin}</div>
                            <div>{this.state.products[key].fiveMinSold}</div>
                            <div>{this.state.products[key].tenMinStock}</div>
                            
                            <div>${this.state.products[key].twentyMin}</div>
                            <div>{this.state.products[key].twentyMinSold}</div>
                            <div>{this.state.products[key].twentyMinStock}</div>
                    
                            <DeleteProduct id={this.state.products[key]._id} allProducts={this.allProducts.bind(this)}/> 
                            <UpdateProductPopup product={this.state.products[key]} allProducts={this.allProducts.bind(this)}/>
                            
        
                    
                  </div > 
          </div>
      )
          })
        }
    </div>
    
  )
}



// <h5>Product: {this.state.products[key].name}</h5>
// <p>Categorie:{this.state.products[key].categorie}</p>
// <p>Color: {this.state.products[key].color}</p>
// <p>Price: </p>
// <p>In stock: {this.state.products[key].stock}</p>

// <div className="header">
// <h1>Charles</h1>
// <div className="tabs">
//     <h3>New Collection</h3>
//     <h3>Pumps</h3>
//     <h3>Boots</h3>
//     <h3>Sneakers</h3>
// </div>

// </div>



// render () {
//   return (
//     <div>
//        {for (var i = 0; i<this.state.length; i++) {
//         return (
//           <div>
//               <h1>Product: {this.state.products.key}</h1>
//               {/* <p>Color: {this.state.products[key].color}</p>
//               <p>In stock: {this.state.products[key].stock}</p>
//               <p>Categorie:{this.state.products[key].categorie}</p> */}
//           </div>
//             )
//        }}
//   </div>)
//   }
                 

                  
            
          
}

  

export default Administrator;
