import React from 'react';
import axios from 'axios'; 
import './App.css';

import Change from './logos/change.png'
import UpdateProduct from './UpdateProduct'


class UpdateProductPopup extends React.Component{

state = {
  showPopup:false
}

togglePopup () {
  this.setState({
      showPopup: !this.state.showPopup
  })
  // this.props.allProducts () this doesn't work here.... why not?? Now the page doesn't refresh automatically
}

    render() {
      
        return (
          <div>
          <button className="table-button" 
                  onClick={this.togglePopup.bind(this)}>
                  <img className="icon-table" src={Change} height="20"/>
          </button>
            {this.state.showPopup ? 
              <div className='popup'>
                <div className='popup-inner'>
                      <h1>Update item</h1>
                                
                                <UpdateProduct product={this.props.product} togglePopup={this.togglePopup.bind(this)}/> 
                                <div className="button-under"> 
                                <button className="small-button" 
                                        onClick={this.togglePopup.bind(this)}>
                                        Back</button>
                                </div>
                </div>  
              </div>
              
          : null
            }
    
    </div>
    ) 
    
}

};


export default UpdateProductPopup