import React from 'react';
import axios from 'axios'; 
import Header from '../Header'; 
import { NavLink } from "react-router-dom"; 
import  { Redirect } from 'react-router-dom'

class Login extends React.Component {
state={
    email:'',
    password:'',
    message:'', 
    fromData: {
        email:'',
        password: '',
        administrator: false,
    }
}

updateEmail (event) {
    const email = event.target.value;
    this.setState({
        email: email
    })
}

updatePassword (event) {
    const password = event.target.value;
    this.setState({
        password: password
    })
}

checkAccount = async event => {
        event.preventDefault();
        const { email } = this.state
        try{
            const response = await axios.get(`${'http://localhost:3001/routes/users/'+email}`)
            let newFromData = {
                email:  response.data.email,
                password: response.data.password,
                administrator: response.data.administrator
            }
            this.setState({
                fromData: newFromData
        })
 
        }

        catch( error ){
            this.setState({
                message: "Your password and/or email adress is/are incorrect"
            })
        }
        if (this.state.email !== '' && this.state.password !== '') {
            if ( this.state.email === this.state.fromData.email 
                && this.state.password === this.state.fromData.password
                    && this.state.fromData.administrator === true ) {
                        this.setState({
                            message: 'Entered as administrator'
                        })
                    }
            else if (this.state.email === this.state.fromData.email 
                && this.state.password === this.state.fromData.password
                    && this.state.fromData.administrator === false) {
                        this.setState({
                            message: 'Welcome Back'
                        })
                        localStorage.setItem("email", this.state.email)
                        localStorage.setItem("loggedIn", true)
                        console.log('storage in checkAccount Login', localStorage)
                    }
                    
            else {
                this.setState ({
                    message:'Your password or email is incorrect'
                })
                localStorage.setItem("email", '')
                localStorage.setItem("loggedIn", false)
            }
           }
        }    
 
    
              
    render (){
        return (
         
            <div className="Login-popup">
            <div className="text-title-left">Log in</div>
                <form onSubmit={this.checkAccount.bind(this)} className="Login-popup-grid">
                   <div className="text-normal">Email:</div>  
                   <input onChange={this.updateEmail.bind(this)}></input>
                    <div className="text-normal">Password:</div> 
                    <input onChange={this.updatePassword.bind(this)}></input>
                    <div></div>
                    <div>
                            <button onClick={this.checkAccount.bind(this)} className="small-button-right">Submit</button>
                            <NavLink to="/">  
                                        <button className="small-button-right">Back</button>
                            </NavLink>
                           
                    </div>    
                 </form> 
                 
                
                <div className="text-message">{this.state.message}</div>

                
                {this.state.message === "Entered as administrator" ?
                     <Redirect to='/admin' /> : null}
                 {/* {this.state.message === "Welcome Back" ?
                        <Redirect to={{
                            pathname: '/',
                            // state: { email: localStorage.getItem('email'), 
                            //     loggedIn: true }
                        }} /> : null} */}
                                    
               
            </div>
        )

//         <Redirect to={{
//             pathname: '/order',
//             state: { id: '123' }
//         }}
// />
    }
    
}
export default Login