import React from 'react';
import axios from 'axios'; 

import { NavLink } from "react-router-dom"; 


class LoadProducts extends React.Component {
    state={
        products : [],
        chart: {}, 
    }

     componentDidMount = async products => {
            let url='http://localhost:3001/routes/products/';
            this.setState ({
                products:[],
                }) 
            try {
                const res = await axios.get(url);
                let copyProducts = [...this.state.products]; 
                for(var i=0; i<res.data.length; i++) {
                    copyProducts.push(res.data[i])
                }
            this.setState({
                    products : copyProducts
                    }) 
                }
            catch(error){
                this.setState({error:'something went wrong'})
                }

            this.state.products.map((item, key) => {
                if (this.state.products[key].name === this.props.name)  { 
                    this.setState({
                        chart: this.state.products[key]
                    })
                    }
                })
     }
    
    handleSubmitFive = () => {
         const { name, fiveMin } = this.state.chart;
         const oldProducts = JSON.parse(localStorage.getItem('productsArray')) || []; 
         var newProduct = {'name' : name , 'date':'fiveMin', 'fiveMin': fiveMin, 'amount':1}
         console.log(newProduct)
         oldProducts.push(newProduct)
         localStorage.setItem('productsArray', JSON.stringify(oldProducts))
    }

    handleSubmitTen= () => {
        const { name, tenMin } = this.state.chart;
        const oldProducts = JSON.parse(localStorage.getItem('productsArray')) || []; 
        var newProduct = {'name' : name , 'date':'tenMin', 'tenMin': tenMin, 'amount':1}
        console.log( newProduct )
        oldProducts.push(newProduct)
        localStorage.setItem('productsArray', JSON.stringify(oldProducts))
        console.log(localStorage)
        }

    handleSubmitTwenty = () => {
    const { name, twentyMin } = this.state.chart;
    const oldProducts = JSON.parse(localStorage.getItem('productsArray')) || []; 
    var newProduct = {'name' : name , 'date':'twentyMin', 'twentyMin': twentyMin, 'amount':1}
    console.log( newProduct )
    oldProducts.push(newProduct)
    localStorage.setItem('productsArray', JSON.stringify(oldProducts))

    }


render () {

    window.scroll({
        top: 0, 
        left: 0, 
      });

        return (
            <div>
      
     
   
                            <div className="coffee-drink-grid">
                            <div className="line-bottom">
                                <div className="text-people-grid-header-left">Expresso date <br /> - 5min - <br /></div>
                                <div className="text-people-grid-header-left">${this.state.chart.fiveMin}</div>
                                <NavLink to="Chart"><button onClick={this.handleSubmitFive.bind(this)} className="middle-button">Book</button></NavLink>
                            </div>
                            <div className="line-bottom">
                                <div className="text-people-grid-header-left">Cappuciono date <br /> - 10min - <br /></div>
                                <div className="text-people-grid-header-left">${this.state.chart.tenMin}</div>
                                <NavLink to="Chart"><button className="middle-button" onClick={this.handleSubmitTen.bind(this)}>Book</button></NavLink>
                            </div>
                
                            <div className="line-bottom">
                                <div className="text-people-grid-header-left">Frapucciono date<br />  - 20min - <br /></div>
                                <div className="text-people-grid-header-left">${this.state.chart.twentyMin}</div>
                                <NavLink to="Chart"><button className="middle-button" onClick={this.handleSubmitTwenty.bind(this)}>Book</button></NavLink>
                            </div>
        
                            </div> 
                        
                
                        </div>
              
         )}

            
                
             
}   

export default LoadProducts; 






// import React from 'react';
// import axios from 'axios'; 


// class LoadProducts extends React.Component {
//     state={
//         products : [],
//     }

//      componentDidMount = async products => {
//             let url='http://localhost:3001/routes/products/';
//             this.setState ({
//                 products:[],
//                 }) 
//             try {
//                 const res = await axios.get(url);
//                 let copyProducts = [...this.state.products]; 
//                 for(var i=0; i<res.data.length; i++) {
//                     copyProducts.push(res.data[i])
//                 }
//             this.setState({
//                     products : copyProducts
//                     }) 
//                 }
                
            
//             catch(error){
//                 this.setState({error:'something went wrong'})
//                 }
//             }
    
//         goToChart() {
//             return (
//                 <Chart /> 
//             )
//         }

// render () {
// return (
   
          
//                 this.state.products.map((item, key) => {
          
//                     if (this.state.products[key].name === this.props.name)  { 
//                         return (
//                             <div className="coffee-drink-grid">
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Expresso date <br /> - 5min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.products[key].fiveMin}</div>
//                                 <button onClick={this.goToChart} className="middle-button">Book</button>
//                             </div>
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Cappuciono date <br /> - 10min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.products[key].tenMin}</div>
//                                 <button onClick={this.goToChart} className="middle-button">Book</button>
//                             </div>
                
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Frapucciono date<br />  - 20min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.products[key].twentyMin}</div>
//                                 <button onClick={this.goToChart} className="middle-button">Book</button>
//                             </div>
//                             </div> 
//                         )}  else { return (null) }
                
               
              
//                 })
// )
//             }
                
             
// }   

// export default LoadProducts; 




// import React from 'react';
// import axios from 'axios'; 
// import Header from '../Header';
// import { NavLink } from "react-router-dom"; 


// class LoadProducts extends React.Component {
//     state={
//         products : [],
//         chart: {}, 
//     }

//      componentDidMount = async products => {
//             let url='http://localhost:3001/routes/products/';
//             this.setState ({
//                 products:[],
//                 }) 
//             try {
//                 const res = await axios.get(url);
//                 let copyProducts = [...this.state.products]; 
//                 for(var i=0; i<res.data.length; i++) {
//                     copyProducts.push(res.data[i])
//                 }
//             this.setState({
//                     products : copyProducts
//                     }) 
//                 }
//             catch(error){
//                 this.setState({error:'something went wrong'})
//                 }

//             this.state.products.map((item, key) => {
//                 if (this.state.products[key].name === this.props.name)  { 
//                     this.setState({
//                         chart: this.state.products[key]
//                     })
//                     }
//                 })
//             }
    
//     handleSubmitFive = () => {
//          const { name, fiveMin, fiveMinSold, fiveMinStock } = this.state.chart;
//          console.log( name, fiveMin )
//          localStorage.setItem ('name', name); 
//          localStorage.setItem ('date', 'fiveMin')
//          localStorage.setItem ('fiveMin', fiveMin); 

//         }

//     handleSubmitTen= () => {
//         const { name, tenMin, tenMinSold, tenMinStock } = this.state.chart;
//         var product10 = {'name' : name , 'date':'tenMin', 'tenMin': tenMin}
//         console.log( product10 )
//         localStorage.setItem('product10', JSON.stringify(product10))

//         }

//     handleSubmitTwenty = () => {
//     const { name, twentyMin, twentyMinSold, twentyMinStock } = this.state.chart;
//     var product20 = {'name' : name , 'date':'twentyMin', 'twentyMin': twentyMin}
//     console.log( product20 )
//     localStorage.setItem('product20', JSON.stringify(product20))
//     // localStorage.setItem ('name', name); 
//     // localStorage.setItem ('date', 'twentyMin')
//     // localStorage.setItem ('twentyMin', twentyMin)

//     }


// //     var testObject = { 'one': 1, 'two': 2, 'three': 3 };

// // // Put the object into storage
// // localStorage.setItem('testObject', JSON.stringify(testObject));

// // // Retrieve the object from storage
// // var retrievedObject = localStorage.getItem('testObject');

// // console.log('retrievedObject: ', JSON.parse(retrievedObject));

// render () {
//         return (
//             <div>
      
     
   
//                             <div className="coffee-drink-grid">
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Expresso date <br /> - 5min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.chart.fiveMin}</div>
//                                 <NavLink to="Chart"><button onClick={this.handleSubmitFive.bind(this)} className="middle-button">Book</button></NavLink>
//                             </div>
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Cappuciono date <br /> - 10min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.chart.tenMin}</div>
//                                 <NavLink to="Chart"><button className="middle-button" onClick={this.handleSubmitTen.bind(this)}>Book</button></NavLink>
//                             </div>
                
//                             <div className="line-bottom">
//                                 <div className="text-people-grid-header-left">Frapucciono date<br />  - 20min - <br /></div>
//                                 <div className="text-people-grid-header-left">${this.state.chart.twentyMin}</div>
//                                 <NavLink to="Chart"><button className="middle-button" onClick={this.handleSubmitTwenty.bind(this)}>Book</button></NavLink>
//                             </div>
        
//                             </div> 
                        
                
//                         </div>
              
//          )}

            
                
             
// }   

// export default LoadProducts; 


