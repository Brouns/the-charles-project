import React from 'react';
import Login from './Login';


import { NavLink } from "react-router-dom"; 



class LoginPopup extends React.Component {

    render () {
        return (
                <div className='popup'>
                      <div className='popup-inner'>                                                  
                          <Login/>
                          <p className="float-right"> 
                          <NavLink to="/newAccount">To create a new account click here</NavLink>
                        </p>                     
                      </div>  
                </div>

        
     
        )
    }
}

export default LoginPopup

