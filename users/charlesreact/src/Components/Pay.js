
import React from 'react';
import Header from '../Header'
import { Elements, StripeProvider } from "react-stripe-elements";
import CheckoutForm from "./CheckoutForm.js";
import pk_test from "./config.js";

class Pay extends React.Component {

    render() {
        console.log(this.props.location.state.totalAmount)
      return (
        <div>
        <Header />

        <StripeProvider apiKey={pk_test}>
          <div className="example">
            <Elements>
              <CheckoutForm totalAmount={this.props.location.state.totalAmount}/>
            </Elements>
          </div>
        </StripeProvider>
        </div>
      );
    }
  }
  
  export default Pay;
  

