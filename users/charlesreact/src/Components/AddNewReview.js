import React from 'react';
import headshot from '../Images/headshot-empty.jpg'

function AddNewReview (props) {
        console.log(props.reviews)
        for (var i =0; i<props.reviews.length; i++) {
            if (props.reviews[i].number % 2 === 0 ){
            return (
            <div className="reviews-new">
                                <div className="text-peope-grid" >
                                    <div className="text-people-grid-header-right-nohover">Coffee date with {props.reviews[i].date}</div> <br /> 
                                    <div className="text-people-grid-right">"{props.reviews[i].message}" <br /><br /> - {props.reviews[i].name}, {props.reviews[i].location} - </div>
                                </div>
        
                                <div className="photo-round" >
                                    <img className="landingspage-people-img-nohover-small" src={headshot} alt="headshot"/>
                                </div>
                            
            </div>
            )


            } else {
                return (
                 
                    <div className="reviews-new">
                            <div  className="photo-round">
                                <img className="landingspage-people-img-nohover-small" src={headshot} alt="headshot"/>
                            </div>
                            <div className="text-peope-grid">
                                <div className="text-people-grid-header-left-nohover">Coffee date with {props.reviews[i].date} </div> <br /> 
                                <div className="text-people-grid-left">"{props.reviews[i].message}" <br /><br /> - {props.reviews[i].name}, {props.reviews[i].location} -</div>
                            </div>
                            <div></div>
                            <div></div>


                        </div>



                )

            }
                        
        }
        return null
    
}
    


export default AddNewReview