import React from 'react';
import { NavLink } from "react-router-dom"; 
import AddNewReview from './AddNewReview'


class NewReview extends React.Component {

    state ={
        reviews:[],
        number: 0
    }
    sendReview (event) {
        event.preventDefault()
        let oldReviews = [ ...this.state.reviews]
        let number = this.state.number + 1 
        const nameInput = event.target.elements.name
        const emailInput = event.target.elements.email
        const locationInput = event.target.elements.location
        const dateInput = event.target.elements.date
        const messageInput = event.target.elements.message
        var data = {name: nameInput.value, email: emailInput.value, location: locationInput.value, date: dateInput.value, message:messageInput.value, number: number}
        console.log(data)
        oldReviews.push(data)
        this.setState({
            reviews:oldReviews,
            message:'Your review will be posted after approval'
        })

        nameInput.value = ""
        emailInput.value = ""
        messageInput.value = ""
        locationInput.value = ""
        dateInput.value=""
        
        // this.props.addNewReview(data)
    }

    render () {
        
        return (
            <div className="personal-page">
                {this.state.date}
                
                <AddNewReview reviews={this.state.reviews}/> 
                <div className="text-red">{this.state.message}</div>
                <br /> <br /> 
                    <div className="header-big">Leave your story </div>
                        <form onSubmit={this.sendReview.bind(this)} className="review-grid">
                            <div className="text-normal">Name:</div>
                            <input required={true} type="text" name="name" />
                            <div className="text-normal">Email:</div>
                            <input required={true} type="email" name="email" />
                            <div className="text-normal">Location:</div>
                            <input required={true} type="text" name="location" />
                            <div className="text-normal">Date with:</div>
                            <input required={true} type="text" name="date" />
                            <div className="text-normal">Your story about your coffeedate:</div>
                            <input className="Input" align = 'top' defaultvalue='Max. 100 characters' required={true} type="text" name="message" maxlength="100"/>
                            <NavLink to="/"><button className="small-button">Back</button></NavLink>
                            <button type="submit" className="small-button">Send</button>
                            
                    </form>
                   
            </div>
        )
    }
}

export default NewReview