import React from 'react';
import Header from '../Header'
import Footer from './Footer' 
import NewReview from './NewReview'
import banner from '../Images/banner children.jpg'
import headshot1 from '../Images/headshot1.jpg'
import headshot2 from '../Images/headshot2.jpg'
import headshot3 from '../Images/headshot3.jpg'
import headshot4 from '../Images/headshot4.jpg'
import headshot5 from '../Images/headshot5.jpg'
import headshot6 from '../Images/Obama_small.jpg'


class Reviews extends React.Component {



addNewReview(props) {
    const { name, email, location, message, date } = props
    let oldReviews = [ ...this.state.reviews]
    let newReview = {
        name: name,
        email: email,
        location: location,
        date: date,
        message: message
    }
    oldReviews.push(newReview)
    this.setState({
        reviews: oldReviews
    })
    
}

    render () {
        console.log(this.state)
        return (
            <div>
                 <Header/> 

             <div className="reviews">
               
                
                <div className="banner"> 
                <img className="banner-photo-middle" src={banner}  alt="banner"/>
                </div>

                <div className="personal-page">
                <div className="header-big">What other people say about The Charles Project</div> 
                </div>
            
                <div className="landingspage-people">
                    <div>
                        <img className="landingspage-people-img-nohover-small" src={headshot1} alt="headshot1"/>
                    </div>
                    <div className="text-peope-grid">
                        <div className="text-people-grid-header-left-nohover">Coffee with Amy Adams</div> <br /> 
                        <div className="text-people-grid-left">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident." <br /> <br />- Tom O'Brien, USA - </div>
                    </div>
                    <div></div>
                    <div></div>



                    <div className="text-peope-grid" >
                        <div className="text-people-grid-header-right-nohover">Lunch with Justin Timberlake</div> <br /> 
                        <div className="text-people-grid-right">“Best day of my life.” <br /> <br /> - Luke Thompson, Ireland - </div>
                    </div>
                    <div className="photo-round" >
                        <img className="landingspage-people-img-nohover-small" src={headshot2} alt="headshot2"/>
                    </div>

                    <div className="photo-round" >
                        <img className="landingspage-people-img-nohover-small" src={headshot3} alt="headshot3"/>
                    </div>
                    <div className="text-peope-grid" >
                        <div className="text-people-grid-header-left-nohover">Cafe Latte with Michelle Obama</div> <br /> 
                        <div className="text-people-grid-left">"Vivamus arcu felis bibendum ut tristique. Pretium lectus quam id leo. Rhoncus mattis rhoncus urna neque viverra justo nec. Cras ornare arcu dui vivamus. Volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend." <br /> <br /> - Maria Recnoviks, Russia - </div>
                    </div>
             
                    <div></div>
                    <div></div>

                  
              
                    <div  className="text-peope-grid">
                        <div className="text-people-grid-header-right-nohover">Thee with Angela Merkel</div><br />
                        <div className="text-people-grid-right">"Eget dolor morbi non arcu risus quis. Lacus sed turpis tincidunt id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames." <br /><br />- Liza Schulkes, Belgium - </div>
                    </div>
                    <div   className="photo-round">
                        <img className="landingspage-people-img-nohover-small" src={headshot4} alt="headshot4"/>
                    </div>
                 
                  
                
    

                    <div className="photo-round">
                        <img className="landingspage-people-img-nohover-small" src={headshot5} alt="headshot5"/>
                    </div>
                    <div className="text-peope-grid">
                        <div className="text-people-grid-header-left-nohover">Espresso with Elon Musk</div><br />
                        <div className="text-people-grid-left">"Ultricies leo integer malesuada nunc vel risus commodo." <br /><br /> - Oriana DuPont, France -</div>
                    </div>
                    <div></div>
                    <div></div>

                    <div  className="text-peope-grid">
                        <div className="text-people-grid-header-right-nohover"> Coffee with Riccardo Muti </div><br />
                        <div className="text-people-grid-right">"Quam elementum pulvinar etiam non quam lacus suspendisse. Tortor id aliquet lectus proin nibh nisl condimentum id. Volutpat lacus laoreet non curabitur gravida arcu ac." <br /><br />- Barack Obama, USA -</div>
                    </div>
                    <div className="photo-round">
                        <img className="landingspage-people-img-nohover-small" src={headshot6} alt="headshot6"/>
                    </div>
            
              
                </div>
                 
            <NewReview addNewReview={this.addNewReview.bind(this)}/> 
              
                
       </div>
       <Footer /> 
     </div>
            
            
        )}
}

export default Reviews