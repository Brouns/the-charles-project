
import React from 'react';
import { NavLink } from "react-router-dom"; 
import Header from '../Header'
import TotalCost from './TotalCost'
import Trashcan from '../logos/trashcan.png'


class Chart extends React.Component{

state = {
    allProductsArray:[ ], 
    totalAmount: 0, 
        };
    

componentDidMount () {
    const allProducts = localStorage.getItem('productsArray') || []; 
    let allProductsArray = JSON.parse(allProducts)
    this.setState({
        allProductsArray,
    }) 
   
}


addOne (index, key) {
    let newAmount = this.state.allProductsArray[key].amount+1 
    let allProducts=[...this.state.allProductsArray]
    allProducts[key].amount = newAmount
    this.setState({
        allProductsArray:allProducts
    }) 

}


minusOne (index, key) {
    let newAmount = this.state.allProductsArray[key].amount-1 
    if (newAmount >= 0  ) {
        let allProducts=[...this.state.allProductsArray]
        allProducts[key].amount = newAmount
        this.setState({
            allProductsArray:allProducts
        }) 
    }
   
}

deleteProduct (index, key) {
    const allProducts = localStorage.getItem('productsArray'); 
    let allProductsArray = JSON.parse(allProducts)
    allProductsArray.splice(key, 1)
    allProductsArray = JSON.stringify(allProductsArray);
    localStorage.setItem("productsArray", allProductsArray);
    this.componentDidMount() 
}

// updateTotalCosts (sum) {
//     const totalAmount = sum
//     console.log(totalAmount)
    
//     //     this.setState({
//     //         totalAmount: newTotal
//     // })
// }
   

//     //When I try to set state, i create a loop... But how can I change that?




    render(){

        window.scroll({
            top: 0, 
            left: 0, 
          });
        return (
            <div>
            <Header /> 
            <div className="personal-page">
            <br /> <div className="header-big">Your selected coffee date(s)</div>
            {this.state.allProductsArray.map((value,key) => {
                  if (this.state.allProductsArray[key].date === 'fiveMin'){
                      return (
                    <div className="chart-grid">
                        <p>The expresso date with: {this.state.allProductsArray[key].name} <br /> (5 min)</p> 
                          <p>Cost: $ {this.state.allProductsArray[key].fiveMin}</p>
                          <div className="center">
                            <p> Amount: {this.state.allProductsArray[key].amount} </p>
                            <button className="small-button" onClick={(index) => this.minusOne(index, key) }  >-</button>
                            <button className="small-button" onClick={(index) => this.addOne(index, key)} >+</button>
                       </div>
                       <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
            
                  </div>
                      )
                  }
                  else if (this.state.allProductsArray[key].date === 'tenMin') {
                      return (
                        <div className="chart-grid">
                                   <p>The capucciono date with: {this.state.allProductsArray[key].name} <br />  (10 min)</p> 
                                    <p>Cost: $ {this.state.allProductsArray[key].tenMin}</p>

                                    <div className="center">
                                    <p> Amount: {this.state.allProductsArray[key].amount} </p>
                                    <button className="small-button" onClick={(index) => this.minusOne(index, key) }>-</button>
                                    <button className="small-button" onClick={(index) => this.addOne(index, key)}>+</button>
                                    </div>
                                    <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
                                 </div>
                      )
                  }
                  else if (this.state.allProductsArray[key].date === 'twentyMin') {
                    return (
                      <div className="chart-grid">
                                 <p>The frapaciono date with: {this.state.allProductsArray[key].name} <br />  (20 min)</p> 
                                  <p>Cost: $ {this.state.allProductsArray[key].twentyMin}</p>

                                  <div className="center">
                                  <p> Amount: {this.state.allProductsArray[key].amount} </p>
                                  <button className="small-button" onClick={(index) => this.minusOne(index, key) }>-</button>
                                  <button className="small-button" onClick={(index) => this.addOne(index, key)}>+</button>
                                  </div>
                                  <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
                               </div>
                    )
                }
            })}
        
            {Array.isArray(this.state.allProductsArray) && this.state.allProductsArray.length !== 0? 
                
                    <div>
         
                        {console.log(this.state.totalAmount)}
                        <TotalCost allProductsArray={this.state.allProductsArray}/>  
                            {/* <div className="buttons-under">
                                <NavLink to= {{
                                    pathname: '/pay',
                                    state: {
                                        totalAmount : this.state.totalAmount
                                    }
                                }}><button className="big-button">Pay</button></NavLink>     */}
                            <NavLink to="/" ><button className="small-button">Back</button></NavLink>
                    
                            </div>
                    // </div>
             : null } 
 
                
            </div>
            </div>
           
        )
    }
}

export default Chart













// import React from 'react';
// import { NavLink } from "react-router-dom"; 
// import Header from '../Header'
// import TotalCost from './TotalCost'
// import Trashcan from '../logos/trashcan.png'

// class Chart extends React.Component{

// state = {
//     allProductsArray:[], 
//     totalAmount: 2000, 
//         };
    

// componentDidMount () {
//     const allProducts = localStorage.getItem('productsArray') || []; 
//     let allProductsArray = JSON.parse(allProducts)
//     this.setState({
//         allProductsArray,
//     }) 
   
// }


// addOne (index, key) {
//     let newAmount = this.state.allProductsArray[key].amount+1 
//     let allProducts=[...this.state.allProductsArray]
//     allProducts[key].amount = newAmount
//     this.setState({
//         allProductsArray:allProducts
//     }) 

// }


// minusOne (index, key) {
//     let newAmount = this.state.allProductsArray[key].amount-1 
//     if (newAmount >= 0  ) {
//         let allProducts=[...this.state.allProductsArray]
//         allProducts[key].amount = newAmount
//         this.setState({
//             allProductsArray:allProducts
//         }) 
//     }
   
// }

// deleteProduct (index, key) {
//     const allProducts = localStorage.getItem('productsArray'); 
//     let allProductsArray = JSON.parse(allProducts)
//     allProductsArray.splice(key, 1)
//     allProductsArray = JSON.stringify(allProductsArray);
//     localStorage.setItem("productsArray", allProductsArray);
//     this.componentDidMount() 
// }


// updateTotalCosts (sum) {
//     // const newTotal = sum
//     // console.log(newTotal)

//     // this.setState({
//     //     totalAmount: newTotal
//     // })
// }


//     render(){
//         return (
//             <div>
//             <Header /> 
//             <div className="personal-page">
//             <br /> <div className="header-big">Your selected coffee date(s)</div>
//             {this.state.allProductsArray.map((value,key) => {
//                   if (this.state.allProductsArray[key].date === 'fiveMin'){
//                       return (
//                     <div className="chart-grid">
//                         <p>The expresso date with: {this.state.allProductsArray[key].name} <br /> (5 min)</p> 
//                           <p>Cost: $ {this.state.allProductsArray[key].fiveMin}</p>
//                           <div className="center">
//                             <p> Amount: {this.state.allProductsArray[key].amount} </p>
//                             <button className="small-button" onClick={(index) => this.minusOne(index, key) }  >-</button>
//                             <button className="small-button" onClick={(index) => this.addOne(index, key)} >+</button>
//                        </div>
//                        <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
            
//                   </div>
//                       )
//                   }
//                   else if (this.state.allProductsArray[key].date === 'tenMin') {
//                       return (
//                         <div className="chart-grid">
//                                    <p>The capucciono date with: {this.state.allProductsArray[key].name} <br />  (10 min)</p> 
//                                     <p>Cost: $ {this.state.allProductsArray[key].tenMin}</p>

//                                     <div className="center">
//                                     <p> Amount: {this.state.allProductsArray[key].amount} </p>
//                                     <button className="small-button" onClick={(index) => this.minusOne(index, key) }>-</button>
//                                     <button className="small-button" onClick={(index) => this.addOne(index, key)}>+</button>
//                                     </div>
//                                     <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
//                                  </div>
//                       )
//                   }
//                   else if (this.state.allProductsArray[key].date === 'twentyMin') {
//                     return (
//                       <div className="chart-grid">
//                                  <p>The frapaciono date with: {this.state.allProductsArray[key].name} <br />  (20 min)</p> 
//                                   <p>Cost: $ {this.state.allProductsArray[key].twentyMin}</p>

//                                   <div className="center">
//                                   <p> Amount: {this.state.allProductsArray[key].amount} </p>
//                                   <button className="small-button" onClick={(index) => this.minusOne(index, key) }>-</button>
//                                   <button className="small-button" onClick={(index) => this.addOne(index, key)}>+</button>
//                                   </div>
//                                   <button className="table-button" onClick={(index) => this.deleteProduct(index,key)}><img  src={Trashcan} height="20"/></button>
//                                </div>
//                     )
//                 }
//             })}

//                 <TotalCost updateTotalCosts={this.updateTotalCosts} allProductsArray={this.state.allProductsArray}/>  
//                 <div className="buttons-under">
//                 <NavLink to= {{
//                     pathname: '/pay',
//                     state: {
//                         totalAmount : this.state.totalAmount
//                     }
//                 }}><button className="big-button">Pay</button></NavLink>    
//                 <NavLink to="/" ><button className="small-button">Back</button></NavLink>
                
//                 </div>
                
//             </div>
//             </div>
           
//         )
//     }
// }

// export default Chart



      


// import React from 'react';
// import { NavLink } from "react-router-dom"; 
// import Header from '../Header'

// class Chart extends React.Component{

//     state = {
//     }

// componentDidMount () {
//     const name = localStorage.getItem('name') 
//     const fiveMin = localStorage.getItem('fiveMin') 
//     const tenMin = localStorage.getItem('tenMin') 
//     const twentyMin = localStorage.getItem('twentyMin') 
//     console.log('twentyMin: ', JSON.parse(twentyMin))
//     const date = localStorage.getItem('date')
//     const amount = 1
//     this.setState({
//         name, fiveMin, tenMin, twentyMin, date, amount 
//     })
// }




// // // Retrieve the object from storage
// // var retrievedObject = localStorage.getItem('testObject');

// // console.log('retrievedObject: ', JSON.parse(retrievedObject));

// addOne () {
//     this.setState({
//         amount: this.state.amount + 1
//     }) 
// }

// minusOne () {
//     let newAmount = this.state.amount -1 
//     if (newAmount>= 0 ){
//         this.setState({
//             amount: this.state.amount -1 
//         }) 
//     }

// }
//     render(){
//         return (
//             <div>
//             <Header /> 
//             <div className="personal-page">
//             <br /> <div className="header-big">Your selected coffee date(s)</div>
//                     {this.state.date === 'fiveMin' ? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br /> (20 min)</p> 
//                                 <p>Cost: $ {this.state.fiveMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.fiveMin)}</p>
//                             </div>
//                         : this.state.date === 'tenMin' ? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br />  (20 min)</p> 
//                                 <p>Cost: $ {this.state.tenMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.tenMin)}</p>
//                             </div>
//                         : this.state.date === 'twentyMin'? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br />  (20 min)</p> 
//                                 <p>Cost: $ {this.state.twentyMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.twentyMin)}</p>
//                             </div>
//                     : null
//                 }
//                 <NavLink to="/" ><button className="small-button">Back</button></NavLink>
//                 <button className="big-button">Book</button>
//             </div>
//             </div>
           
//         )
//     }
// }

// export default Chart

      









      


// import React from 'react';
// import { NavLink } from "react-router-dom"; 
// import Header from '../Header'

// class Chart extends React.Component{

//     state = {
//     }

// componentDidMount () {
//     const name = localStorage.getItem('name') 
//     const fiveMin = localStorage.getItem('fiveMin') 
//     const tenMin = localStorage.getItem('tenMin') 
//     const twentyMin = localStorage.getItem('twentyMin') 
//     console.log('twentyMin: ', JSON.parse(twentyMin))
//     const date = localStorage.getItem('date')
//     const amount = 1
//     this.setState({
//         name, fiveMin, tenMin, twentyMin, date, amount 
//     })
// }




// // // Retrieve the object from storage
// // var retrievedObject = localStorage.getItem('testObject');

// // console.log('retrievedObject: ', JSON.parse(retrievedObject));

// addOne () {
//     this.setState({
//         amount: this.state.amount + 1
//     }) 
// }

// minusOne () {
//     let newAmount = this.state.amount -1 
//     if (newAmount>= 0 ){
//         this.setState({
//             amount: this.state.amount -1 
//         }) 
//     }

// }
//     render(){
//         return (
//             <div>
//             <Header /> 
//             <div className="personal-page">
//             <br /> <div className="header-big">Your selected coffee date(s)</div>
//                     {this.state.date === 'fiveMin' ? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br /> (20 min)</p> 
//                                 <p>Cost: $ {this.state.fiveMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.fiveMin)}</p>
//                             </div>
//                         : this.state.date === 'tenMin' ? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br />  (20 min)</p> 
//                                 <p>Cost: $ {this.state.tenMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.tenMin)}</p>
//                             </div>
//                         : this.state.date === 'twentyMin'? 
//                             <div className="chart-grid">
//                                 <p>The frapaciono date with: {this.state.name} <br />  (20 min)</p> 
//                                 <p>Cost: $ {this.state.twentyMin}</p>
//                                 <div className="center">
//                                 <p> Amount: {this.state.amount} </p>
//                                 <button className="small-button" onClick={this.minusOne.bind(this)}>-</button>
//                                 <button className="small-button" onClick={this.addOne.bind(this)}>+</button>
//                                 </div>
//                                 <p> Total: $ {this.state.amount * parseInt(this.state.twentyMin)}</p>
//                             </div>
//                     : null
//                 }
//                 <NavLink to="/" ><button className="small-button">Back</button></NavLink>
//                 <button className="big-button">Book</button>
//             </div>
//             </div>
           
//         )
//     }
// }

// export default Chart

      