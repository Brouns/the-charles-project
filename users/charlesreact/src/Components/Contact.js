import React from 'react';
import axios from 'axios'; 

import Header from '../Header';
import { NavLink } from "react-router-dom"; 

class Contact extends React.Component {

    state = {
        title:"How can we help you?"
    }
    

    sendMessage(event) {
        console.log("send message")
        event.preventDefault()
        const nameInput = event.target.elements.name
		const emailInput = event.target.elements.email
		const messageInput = event.target.elements.message
        const subjectInput = event.target.elements.subject 
        var data = {name: nameInput.value, email: emailInput.value, message:messageInput.value, subject:subjectInput.value}
        console.log(data)

        
        axios.post('http://localhost:3001/routes/sendEmail', data)
		.then((response) => {
			nameInput.value = ""
			emailInput.value = ""
            messageInput.value = ""
            subjectInput.value = ""
            alert("Your message has been sent, thanks!")
            console.log(response)
		})
		.catch(function (error) {
			console.log(error);
		})
		console.log("--SeNd!--")
    }


    render () {
        return (
            <div>
            <Header /> 
            <div className="personal-page">
            
                        <div className="text-title-left">
                        {this.state.title}
                        </div>

                        <form onSubmit={this.sendMessage.bind(this)} className="contact-grid">
                            <div className="text-normal">Name:</div>
                            <input required={true} type="text" name="name" />
                            <div className="text-normal">Email:</div>
                            <input required={true} type="email" name="email" />
                            <div className="text-normal">Subject:</div>
                            <input required={true} type="text" name="subject" />
                            <div className="text-normal">Message:</div>
                            <input required={true}  name="message" />
                            <NavLink to="/"><button className="small-button">Back</button></NavLink>
                            <button type="submit" className="small-button">Submit</button>
                            
                        </form>
            </div>
            
            
            
            </div>
        )
    }
}

export default Contact









// sendMessage (event) {
//     var that = this
//     event.preventDefault()
//     const nameInput = event.target.elements.name
//     const emailInput = event.target.elements.email
//     const messageInput = event.target.elements.message
//     const subject = that.props.subject 
//     var data = {name: nameInput.value, email: emailInput.value, message:messageInput.value, subject:subject}
//     console.log(data)

//     axios.post('http://localhost:3001/routes/sendEmail', data)
//     .then((response) => {
//         nameInput.value = ""
//         emailInput.value = ""
//         messageInput.value = ""
//         alert("Your message has been sent, thanks!")
//     })
//     .catch(function (error) {
//         console.log(error);
//     })
//     console.log("--SeNd!--")
// }
