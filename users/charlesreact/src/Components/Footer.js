import React from 'react'; 
import '../App.css';
import { NavLink } from "react-router-dom"; 


function Footer () {

    return (
        <div className="footer3">
                    <div className="footer">
                    <NavLink className="footer-title" to="/about">About</NavLink>
                    <ul>
                        <li>History</li>
                        <li>Beliefs</li>
                        <li>Project</li>
                        <li>Board</li>
                        <li>Supporters</li>
                    </ul>
                    </div>

                    <div className="footer">
                    <NavLink className="footer-title" to="/contact">Contact Us</NavLink>
                    <ul>
                        <li>Contact</li>
                        <li>Location</li>
                    </ul>
                    </div>

                    <div className="footer">
                    <NavLink className="footer-title" to="/Q&A"> Q&A </NavLink>
                    <ul>
                        <li>How can I pay?</li>
                        <li>Do I need to be a member?</li>
                    </ul>
                    </div>
                
        </div>
    )
    }
export default Footer 