import React from 'react';

import Header from '../Header';



function Error () {
    return (
        <div>

       
        <Header /> 
        <div className="personal-page">
            <div className="header-big-center">Oops, something went wrong...</div>
        </div>
        </div>
    )

}

export default Error