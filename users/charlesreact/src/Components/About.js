import React from 'react';
import pr_1 from '../Images/pr_1.jpg';
import pr_2 from '../Images/pr_2.jpg';
import pr_3 from '../Images/pr_3.jpg'
import pr_5 from '../Images/pr_5.jpg'
import pr_7 from '../Images/pr_7.jpeg'

import Header from '../Header'

import Footer from './Footer' 
import { NavLink } from "react-router-dom"; 


class About extends React.Component {
    render () {
        window.scroll({
            top: 0, 
            left: 0, 
          });
        return (
            <div>
                <Header /> 
                <div className="banner">
                    <h1>CHARLES</h1>
                    <h3>The Project</h3>
                </div>
                <div className="text-frame">
                    <div className="text-title">Mission</div>
                    <p>Lobortis scelerisque fermentum dui faucibus in. Egestas congue quisque egestas diam in arcu cursus euismod. Sit amet risus nullam eget. Proin libero nunc consequat interdum varius sit amet mattis. Eget lorem dolor sed viverra. Molestie at elementum eu facilisis sed odio morbi quis. Leo in vitae turpis massa sed elementum tempus. Arcu odio ut sem nulla pharetra diam sit amet nisl. Tortor pretium viverra suspendisse potenti nullam. Ut aliquam purus sit amet luctus venenatis lectus magna. Scelerisque in dictum non consectetur a erat nam at lectus. Malesuada fames ac turpis Egestas integer. Volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim. Facilisis gravida neque convallis a cras. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Sed elementum tempus egestas sed sed risus pretium. </p>

                    <div className="quote">
                    “We Become What We Think About.” - Earl Nightingale
                    </div>

                    <div className="text-title">History</div>
                    <p>Lobortis scelerisque fermentum dui faucibus in. Egestas congue quisque egestas diam in arcu cursus euismod. Sit amet risus nullam eget. Proin libero nunc consequat interdum varius sit amet mattis. Eget lorem dolor sed viverra. Molestie at elementum eu facilisis sed odio morbi quis. Leo in vitae turpis massa sed elementum tempus. Arcu odio ut sem nulla pharetra diam sit amet nisl. Tortor pretium viverra suspendisse potenti nullam. Ut aliquam purus sit amet luctus venenatis lectus magna. Scelerisque in dictum non consectetur a erat nam at lectus. Malesuada fames ac turpis Egestas integer. Volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim. Facilisis gravida neque convallis a cras. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Sed elementum tempus egestas sed sed risus pretium. </p>
   
                    <div className="text-title">Projects</div>
                    <p>Lobortis scelerisque fermentum dui faucibus in. Egestas congue quisque egestas diam in arcu cursus euismod. Sit amet risus nullam eget. Proin libero nunc consequat interdum varius sit amet mattis. Eget lorem dolor sed viverra. Molestie at elementum eu facilisis sed odio morbi quis. Leo in vitae turpis massa sed elementum tempus. Arcu odio ut sem nulla pharetra diam sit amet nisl. Tortor pretium viverra suspendisse potenti nullam. Ut aliquam purus sit amet luctus venenatis lectus magna. Scelerisque in dictum non consectetur a erat nam at lectus. Malesuada fames ac turpis Egestas integer. Volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim. Facilisis gravida neque convallis a cras. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Sed elementum tempus egestas sed sed risus pretium. </p>


                </div>
                
        
                <NavLink to="/" href="#coffee-dates">
                <button className="big-button">Click here to see this years coffee dates</button>
                </NavLink>
             

                <div className="cirkels">
                    <div className="cirkel1">
                        <img className="small_round_img" src={pr_1} alt="pr_1"/>
                    </div>
                    <div className="cirkel2">
                        <img className="big_round_img" src={pr_5} alt="pr_5"/>  
                    </div>
                    <div className="cirkel3">
                        <img className="middle_round_img" src={pr_2} alt="pr_2"/> 
                    </div>
                    <div className="cirkel4">
                        <img className="small_round_img" src={pr_7} alt="pr_7"/>
                    </div>
                    <div className="cirkel5">
                        <img className="small_round_img" src={pr_3} alt="pr_3"/>
                    </div>
                </div>

                <Footer /> 
            </div>
        )
    }
}

export default About