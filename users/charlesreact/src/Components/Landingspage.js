import React from 'react';
import Header from '../Header'
import Footer from './Footer' 
import { NavLink } from "react-router-dom"; 


import messi_big_2 from '../Images/messi_big_2.jpg'; 
import Trudeau_bigg from '../Images/Trudeau_bigg.jpg';
import meryl_big from '../Images/meryl_big.jpg';
import meghan_bigg from '../Images/meghan_bigg.jpg'
import Meryl_small from '../Images/Meryl_small.jpg'
import trudeau_small from '../Images/trudeau_small.jpg'
import messi_small from '../Images/messi small.jpg'
import meghan_small from '../Images/meghan_small.jpg'
import donald_small from '../Images/donald_small.jpg'
import Alexandria_small from '../Images/Alexandria_small.jpg'
import '../App.css';


class Landingspage extends React.Component {


    render () {
          window.scroll({
                        top: 0, 
                        left: 0, 
                    });
        return (
            <div >
                {console.log('localstorage in landingspage ', localStorage)}
                <Header /> 
                {/* <Header email={this.props.location.state.email} loggedIn={this.props.location.state.loggedIn } /> */}
   

                <div className="landingspage-grid">
                    <NavLink  className="landingspage-1" to="/LionelMessi">
                        <img className="gallery__img" src={messi_big_2} alt="messi-big"/>
                    </NavLink>

                    <div className="landingspage-2">
                        <div className="text-normal" >We already collected </div>
                        <div className="text-big">$72.167</div> 
                        <div className="text-normal">this year!</div>
                    </div>

                    <NavLink className="landingspage-3" to="/about">
                        <div className="headerCharles">
                            <h1>CHARLES</h1>
                            <h3>the project</h3>
                        </div>
                    </NavLink>

                    <NavLink className="landingspage-4" to="/about">
                        <div className="text-normal">
                            <u>Click here</u> to learn more about <br /> <br /> 
                            <b>The Charles Project</b>
                        </div>
                    </NavLink>
                    <NavLink to="/JustinTrudeau" className="landingspage-5">
                        <img className="gallery__img" src={Trudeau_bigg} alt="trudeau-big"/>
                    </NavLink>

                    
                    <a href="#coffee-dates" className="landingspage-6">
                            <div className="text-normal"><u>Buy here</u> your coffee date to support<br /> <br /> <b>The Charles Project</b></div> 
                    </a> 
                    
                    
                    <NavLink to="/Contact" className="landingspage-7">
                        <div className="text-normal"><u>Ask</u> a question</div>
                    </NavLink>

                    <NavLink to="/MerylStreep" className="landingspage-8">
                        <img className="gallery__img" src={meryl_big} alt="meryl-big"/>
                    </NavLink>

                    <NavLink to="/reviews" className="landingspage-9">
                        <div className="text-normal">Read last years' reviews</div>
                    </NavLink>

                    <NavLink to="/MeghanMarkle" className="landingspage-10">
                        <img className="gallery__img" src={meghan_bigg} alt="meghan-big"/>
                    </NavLink>
                </div>






                <div id="coffee-dates" className="header-big">Go on a coffee date and support the Charles Project!</div>




                <div className="landingspage-people">
                    <NavLink to="/MerylStreep" className="photo-round-big">
                        <img className="landingspage-people-img" src={Meryl_small} alt="Meryl Streep"/>
                    </NavLink>
                    <NavLink to="/MerylStreep" className="text-peope-grid">
                        <div className="text-people-grid-header-left">Meryl Streep</div>
                        <div className="text-people-grid-left">"Meryl Streep is exactly as awesome as you would imagine Meryl Streep to be." <br /> - Michelle Obama</div>
                    </NavLink>
                    <div></div>
                    <div></div>



                    <NavLink className="text-peope-grid" to="/DonaldGlover">
                        <div className="text-people-grid-header-right">Donald Glover</div>
                        <div className="text-people-grid-right">“This is one of the great performers in modern America.” <br /> - TV host Touré</div>
                    </NavLink>
                    <NavLink className="photo-round-big" to="/DonaldGlover">
                        <img className="landingspage-people-img" src={donald_small} alt="Donald Glover"/>
                    </NavLink>

                    <NavLink className="photo-round-big" to="/MeghanMarkle">
                        <img className="landingspage-people-img" src={meghan_small} alt="Meghan Markle"/>
                    </NavLink>
                    <NavLink className="text-peope-grid" to="/MeghanMarkle">
                        <div className="text-people-grid-header-left">The Duchess of Sussex</div>
                        <div className="text-people-grid-left">"Easily one of the most facination people of 2020." <br /> - Elton John </div>
                    </NavLink>
             
                    <div></div>
                    <div></div>

                  
              
                    <NavLink  className="text-peope-grid"to="/LionelMessi">
                        <div className="text-people-grid-header-right">Lionel Messi</div>
                        <div className="text-people-grid-right">"Messi is the best in the world without any doubt and for me the history of football." <br />- Luis Enrique </div>
                    </NavLink>
                    <NavLink  className="photo-round-big" to="/LionelMessi">
                        <img className="landingspage-people-img" src={messi_small} alt="Lionel Messi"/>
                    </NavLink>
                 
                  
                
    

                    <NavLink to="/Alexandria Ocasio-Cortez" className="photo-round-big">
                        <img className="landingspage-people-img" src={Alexandria_small} alt="Alexandra"/>
                    </NavLink>
                    <NavLink to="/Alexandria Ocasio-Cortez" className="text-peope-grid">
                        <div className="text-people-grid-header-left">Alexandria Ocasio-Cortez</div>
                        <div className="text-people-grid-left">"She took on the entire local Democratic establishment in her district and won a very strong victory." <br /> - Bernie Sanders</div>
                    </NavLink>
                    <div></div>
                    <div></div>

                    <NavLink to="JustinTrudeau" className="text-peope-grid">
                        <div className="text-people-grid-header-right">Justin Trudeau</div>
                        <div className="text-people-grid-right">"Trudeau’s idea of being prime minister is to be out there listening to Canadians and engaging Canadians" <br />- Nik Nanos</div>
                    </NavLink>
                    <NavLink to="JustinTrudeau" className="photo-round-big">
                        <img className="landingspage-people-img" src={trudeau_small} alt="Justin Trudeau"/>
                    </NavLink>
            
                </div>

                <Footer /> 
                
            </div>
        )
    }
}

export default Landingspage