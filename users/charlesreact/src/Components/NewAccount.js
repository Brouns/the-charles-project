import React from 'react';
import Footer from './Footer'; 
import axios from 'axios'; 
import { NavLink } from "react-router-dom"; 
import Header from '../Header'; 
import  { Redirect } from 'react-router-dom'

class NewAccount extends React.Component{
    state={
    accountCreated: false, 
    message:''
    }



 addAccount = async event => {
    event.preventDefault()

            const firstnameInput = event.target.elements.firstname
            const lastnameInput = event.target.elements.lastname
            const emailInput = event.target.elements.email
            const password1Input = event.target.elements.password1
            const password2Input = event.target.elements.password2
            var newAccount = {firstname: firstnameInput.value, lastname: lastnameInput.value, email: emailInput.value, password1: password1Input.value, password2: password2Input.value}
            console.log(newAccount)
            
            if( !emailInput || !password1Input || !password2Input || !firstnameInput ||!lastnameInput) {
                this.setState({
                    message: "All fields are required"
                })
            } else if (password1Input.value !== password2Input.value) { 
                this.setState({
                message: "Passwords must match"
                })}
            else {
                try{
                    const firstname = newAccount.firstname
                    const lastname = newAccount.lastname
                    const email = newAccount.email
                    const password = newAccount.password
                    console.log(firstname, lastname, email, password)
                const response = await axios.post(`${'http://localhost:3001/routes/users/new'}`,{firstname, lastname, email, password})
                console.log(response)
                alert("Your new account has been created")
                localStorage.setItem("email", email)
                localStorage.setItem("loggedIn", true)
                firstnameInput.value = ""
                lastnameInput.value = ""
                emailInput.value = ""
                password1Input.value = ""
                password2Input.value=""
                this.setState({
                    accountCreated:true
                })
                
                
                }
                catch( error ){
                    console.log(error)
                  }}

           
            
 }

    render() {
        return (
            <div> 
            <Header /> 
            {this.state.accountCreated === true ? 
                <Redirect to="/"></Redirect>
                : null 
            }
            <div className="personal-page">
                <br /> <div className="header-big"> Register here for the Charles Project</div>
                <form onSubmit={this.addAccount.bind(this)} className="grid-newaccount-border-all">
                            <div className="text-normal"> Firstname: </div>
                            <input required={true} type="text" name="firstname"></input>

                            <div className="text-normal"> Lastname: </div>  
                            <input required={true} type="text" name="lastname" ></input>
                
                            <div className="text-normal"> Email: </div> 
                            <input required={true} type="email" name="email"></input>
                 
                            <div className="text-normal"> Password: </div>
                            <input required={true} type="text" name="password1" ></input>

                            <div className="text-normal"> Password: </div>
                            <input required={true} type="text" name="password2"></input>
                            
                            <div></div>
                            <p>{this.state.message}</p>
                            
                            <div></div>

                            <div className="alone">
                                <button className="small-button-right">Submit</button>
                                <NavLink to="/"><button className="small-button-right">Back</button></NavLink>
                            </div>
                            
            </form>

            <Footer /> 
            </div>
        </div>
        )
    }
}

export default NewAccount