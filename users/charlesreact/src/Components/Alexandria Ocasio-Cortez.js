import React from 'react';

import alexandria_big from '../Images/alexandria_big.jpg'
import LoadProducts from './LoadProducts'; 
import Header from '../Header'
import Footer from './Footer' 
import { NavLink } from "react-router-dom"; 

class AlexandriaOcasioCortez extends React.Component {
    
    render () {  
        return (
            
            <div>
                <Header />
                    <div className="banner">
                            <img className="banner-photo-70" src={alexandria_big}  alt="Alexandria Ocasio-Cortez"/>
                    </div>

                    <div className="personal-page">
                            <div className="text-title-left">Alexandria Ocasio-Cortez</div>
                                    <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. 
                                    </p>
                                    <NavLink to="/" className="small-button-right">Back</NavLink>
                                    
                            
                                    <LoadProducts name="Alexandria Ocasio-Cortez" /> 
                            
                    </div>

                    < Footer /> 
         </div>
        )
    }
}

export default AlexandriaOcasioCortez