import React from 'react';
import './App.css';
import { NavLink } from "react-router-dom"; 

class HeaderAdmin extends React.Component {

    state={
        email: '' ,
        loggedIn: false,
    }

    componentDidMount (props) {
        this.setState ({
            email : this.props.email,
            loggedIn : this.props.loggedIn
        })

    }

    logOut () {
        this.setState ({
            loggedIn: false, 
        })
    }

    render () {
    return (

        <div className="header">
        
            <NavLink to="/"><h3>CHARLES</h3></NavLink>
           
                {           
                this.state.loggedIn === true ? (
                    <p className="navlink"> Account: {this.state.email} 
                        <NavLink to="/" ><button onClick={this.logOut.bind(this)}>Log Out</button></NavLink> </p> 
                ):  <NavLink className="navlink" to="/login">Log in</NavLink>
                }
             <NavLink className="navlink" to="/users"><button>Users</button></NavLink>
            <NavLink className="navlink" to="/products"><button>Products</button></NavLink>
            
        </div>
    )
            }
}

export default HeaderAdmin





