import React from 'react';
import axios from 'axios'; 
import './App.css';
import AddUsersPopup from './AddUsersPopup'
import HeaderAdmin from './HeaderAdmin'; 
import DeleteUser from './DeleteUser';
import UpdateUserPopup from './UpdateUserPopup'

class Users extends React.Component {

state={
    users : []
  }

  componentDidMount(){
    this.allUsers()
  }


allUsers = async products => {
    let url='http://localhost:3001/routes/users/';
    this.setState ({
      users:[]
    }) 
    try {
          const res = await axios.get(url);
   
          let copyUsers = [...this.state.users]; 
  
          for(var i=0; i<res.data.length; i++) {
            copyUsers.push(res.data[i])
  
          }
      this.setState({
              users : copyUsers
            }) 
        }
       
    catch(error){
        this.setState({error:'something went wrong'})
        }
  
    }
  
  findUsers (props) {

    this.setState({
      users: [props.data]
    })
  }

  selectUsers (props){
    console.log(props)
    this.setState({
      users: props
    })
  }
  
  render () {
  
    return(
      <div>
        <HeaderAdmin email="administrator" loggedIn= {true} /> 
        <div className="headerCharles">
            <h1>CHARLES</h1>
            <h3>The Project</h3>
        </div>
        <AddUsersPopup  selectUsers={this.selectUsers.bind(this)} allUsers={this.allUsers.bind(this)} findUsers={this.findUsers.bind(this)} /> 
        
              <div className="table-header">
                    <div>ID number</div>
                    <div>Firstname</div>

                    <div>Lastname</div>
                    <div></div>
                    <div>Email</div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div>Password</div>
                    <div></div>
                    <div>Admin</div>
                    <div></div>
                    
  
              </div>
      { this.state.users.map((item, key) => {

        return (
              <div className="product">
                   <div className="table-products">
                        {console.log(this.state.users)}
                              <div className="product-classification">{this.state.users[key]._id}</div>
                              <div>{this.state.users[key].firstname}</div>
                     
                              <div>{this.state.users[key].lastname}</div>
                              <div></div>
                              <div>{this.state.users[key].email}</div>
                              <div></div>
                              <div></div>
                              <div></div>
                              <div>{this.state.users[key].password}</div>
                              <div></div>
                              {this.state.users[key].administrator === true ? 
                               <div>True</div>: <div>False</div>}

                            
                                            
                              <DeleteUser id={this.state.users[key]._id} allProducts={this.allUsers.bind(this)}/> 
                              <UpdateUserPopup product={this.state.users[key]} allUsers={this.allUsers.bind(this)}/>
                              
                    </div>
            </div>
        )})
          }

      </div>
    )}
        }
  
export default Users