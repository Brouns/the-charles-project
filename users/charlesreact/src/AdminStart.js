import React from 'react';
import { NavLink } from "react-router-dom";
import Header from './Header' 

function AdminStart () {

    return (
        <div>
            <Header /> 
            <div className="adminStart">
        <NavLink to="/users"><button className="big-button">Users</button></NavLink>
        <NavLink to="/products"><button className="big-button">Products</button></NavLink>
        </div>
        </div>
    )   
}

export default AdminStart