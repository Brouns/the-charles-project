import React from 'react';
import './App.css';
import axios from 'axios'; 

class AddProduct extends React.Component {

state = {
    name: '',
    fiveMin: 0,
    fiveMinSold:0, 
    fiveMinStock: 0,
    
    tenMin: 0,
    tenMinSold:0,
    tenMinStock: 0,

    twentyMin: 0,
    twentyMinSold:0,
    twentyMinStock: 0
}

changeName (event) {
    const newName = event.target.value
    this.setState ({
        name: newName 
    })
}

changeFiveMin (event) {
    const newFiveMin = event.target.value
    this.setState ({
        fiveMin: newFiveMin 
    })
}

changeFiveMinSold (event) {
    const newFiveMinSold = event.target.value
    this.setState ({
        fiveMinSold: newFiveMinSold 
    })
}

changeFiveMinStock (event) {
    const newFiveMinStock = event.target.value
    this.setState ({
        fiveMinStock: newFiveMinStock 
    })
}




changeTenMin (event) {
    const newTenMin = event.target.value
    this.setState ({
        tenMin: newTenMin 
    })
}

changeTenMinSold(event){
    const newTenMinSold = event.target.value
    this.setState ({
        tenMinSold: newTenMinSold
    })
}

changeTenMinStock (event) {
    const newTenMinStock = event.target.value
    this.setState ({
        tenMinStock: newTenMinStock 
    })
}




changeTwentyMin (event) {
    const newTwentyMin = event.target.value
    this.setState ({
        twentyMin: newTwentyMin
    })
}

changeTwentyMinSold(event){
    const newTwentyMinSold = event.target.value
    this.setState ({
        twentyMinSold: newTwentyMinSold
    })
}

changeTwentyMinStock (event) {
    const newTwentyMinStock = event.target.value
    this.setState ({
        twentyMinStock: newTwentyMinStock 
    })
}



    handleSubmit = async event => {
        console.log('handleSubmit')
            event.preventDefault();
            const { name , fiveMin, fiveMinSold, fiveMinStock, tenMin, tenMinSold, tenMinStock, twentyMin, twentyMinSold, twentyMinStock } = this.state
            try{
              const response = await axios.post(`${'http://localhost:3001/routes/products/new'}`,{name, fiveMin, fiveMinSold, fiveMinStock, tenMin, tenMinSold, tenMinStock, twentyMin,twentyMinSold, twentyMinStock})
              console.log(response)
            }
            catch( error ){
              console.log(error)
            }
            this.props.togglePopup()
           }
      

      render () {
          return (
            <div>
            <form onSubmit={this.handleSubmit.bind(this)}>
                                      
                <div className="popup-form"> 
                    <label for="name">Name: </label>
                    <input onChange={this.changeName.bind(this)} type="text" name="name" />  
                    
                    <div></div>
                    <div></div>

                    <div></div>
                    <div></div>

                    <label  for="fiveMin">Costs 5min: </label>
                    <input  onChange={this.changeFiveMin.bind(this)} type="number" name="fiveMin" />

                    <label  for="fiveMinSold">Sold: </label> 
                    <input onChange={this.changeFiveMinSold.bind(this)} type="number" name="fiveMinSold" />

                    <label  for="fiveMinStock">In stock: </label> 
                    <input onChange={this.changeFiveMinStock.bind(this)} type="number" name="fiveMinStock" />


                                                        
                    <label  for="tenMin">Costs 10min: </label>
                    <input onChange={this.changeTenMin.bind(this)} type="number" name="tenMin" />

                    <label  for="tenMinSold">Sold: </label> 
                    <input onChange={this.changeTenMinSold.bind(this)} type="number" name="tenMinSold" />

                    <label for="tenMinStock">In stock: </label>
                    <input onChange={this.changeTenMinStock.bind(this)} type="number" name="tenMinStock" />


                                                        
                    <label  for="twentyMin">Costs 20min: </label>
                    <input onChange={this.changeTwentyMin.bind(this)} type="number" name="twentyMin"  />

                    <label  for="twentyMinSold">Sold: </label> 
                    <input onChange={this.changeTwentyMinSold.bind(this)} type="number" name="twentyMinSold" />

                    <label for="twentyMinStock">In stock: </label>
                    <input onChange={this.changeTwentyMinStock.bind(this)} type="number" name="twentyMinStock" /> 

                    </div>
            </form>
                    <div className="button-under">  
                    <button class="small-button" onClick={this.handleSubmit.bind(this)} >Add</button>
                    </div>
              </div>      
                    

          )}
      
          }

export default AddProduct