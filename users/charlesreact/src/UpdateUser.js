import React from 'react';
import axios from 'axios'; 

class UpdateUser extends React.Component{
 
product = this.props.product

state = {
    firstname: '',
    lastname:'',
    email:'',
    password:'',
    admin: false
}

changeFirstname (event) {
    const newName = event.target.value
    this.setState ({
        firstname: newName 
    })
}

changeLastname (event) {
    const newName = event.target.value
    this.setState ({
        lastname: newName 
    })
}

changeEmail(event) {
    const newEmail = event.target.value
    this.setState ({
        email: newEmail
    })
}

changePassword (event) {
    const newPassword = event.target.value
    this.setState ({
        password: newPassword
    })
}

changeAdmin (event) {
    const newAdmin = event.target.value
    this.setState ({
        administrator: newAdmin 
    })
}


updateUser  = async event => {
    console.log(this.state)
    event.preventDefault();
    const { firstname, lastname, email, password, administrator } = this.state
    const id = this.props.user._id
    console.log(id)
    try{
      const response = await axios.post(`${'http://localhost:3001/routes/users/update'}`,{id, firstname, lastname, email, password, administrator})
    }
    catch( error ){
      console.log(error)
    }
    this.props.togglePopup()
    this.props.allUsers()
   }




render (){
    return (
        <div>
            <form onSubmit={this.updateUser.bind(this)}>
        <div className="popup-form-update"> 
    
                    <label for="firstname">Firstname: </label>
                    <input onChange={this.changeFirstname.bind(this)} type="text" name="firstname" />  

                    <label  for="lastname">Lastname: </label>
                    <input  onChange={this.changeLastname.bind(this)} type="text" name="lastname" />

                    <div></div>
                    <div></div>
                    <label  for="email">Email: </label> 
                    <input onChange={this.changeEmail.bind(this)} type="email" name="email" />

                    <label  for="password">Password: </label> 
                    <input onChange={this.changePassword.bind(this)} type="text" name="password" />

                    <div></div>   
                    <div></div>                         
                    <label  for="admin">Admin: </label>
                    <input onChange={this.changeAdmin.bind(this)} type="bolean" name="admin" />

                    
                    </div>
            </form>
                    <div className="button-under">  
                    <button class="small-button" onClick={this.updateUser.bind(this)} >Update</button>
                    </div>
              </div>
    
    )}

}

export default UpdateUser