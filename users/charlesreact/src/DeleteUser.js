import React from 'react';
import axios from 'axios'; 
import Trashcan from './logos/trashcan.png'

class DeleteUser extends React.Component{

    deleteUser = async event => {
        const id= this.props.id
    
            try{
              const response = await axios.post(`${'http://localhost:3001/routes/users/delete'}`,{id})
              console.log(response)
            }
            catch( error ){
              console.log(error)
            }
            this.props.allProducts ()
           }


    render (){
        return (
            <button className="table-button" onClick={this.deleteUser.bind(this)}><img  src={Trashcan} height="20"/></button>
        )
    }
}




export default DeleteUser