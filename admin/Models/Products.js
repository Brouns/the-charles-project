const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({
        name: {
            type:String,
            required: true,
            unique:true
        },
        fiveMin: {
            type:Number,
            required: true,
            unique:false
        },
        tenMin: {
            type:Number,
            required: true,
            unique:false
        }, 
        twentyMin: {
            type:Number,
            required: true,
            unique:false
        },
        fiveMinStock: {
            type:Number,
            required: true,
            unique:false
        },
        tenMinStock: {
            type:Number,
            required: true,
            unique:false
        }, 
        twentyMinStock: {
            type:Number,
            required: true,
            unique:false
        },
        fiveMinSold: {
            type:Number,
            required:true,
            unique:false
        },
        tenMinSold: {
            type:Number,
            required:true,
            unique:false
        },
        twentyMinSold: {
            type:Number,
            required:true,
            unique:false
        }

})
module.exports = mongoose.model('Products', ProductSchema)