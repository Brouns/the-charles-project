
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({ 
    firstname : {
        type: String,
        required: true,
        unique: false
    },
    lastname : {
        type: String,
        required: true,
        unique: false
    },
    email : {
        type: String,
        required: true,
        unique: true
    },
    password : {
        type: String,
        required: true,
        unique: false
    },
    administrator: {
        type: Boolean, 
        required: true,
    }

})

module.exports = mongoose.model('Users', UserSchema)

