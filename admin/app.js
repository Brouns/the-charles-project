const express = require('express');
const app = express(); 
const mongoose = require('mongoose'); 

mongoose.connect('mongodb://127.0.0.1/charles_DB',()=>{
    console.log('*** connected to mongodb ***');
});


app.use(express.static('public'))

const cors = require("cors");
app.use(cors());
app.options('/routes/sendEmail', cors())
// app.options('/payment/charge', cors())

const bodyParser= require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});

const ProductsRoute = require('./Routes/ProductsRoute')
app.use('/Routes', ProductsRoute)

const UsersRoute = require('./Routes/UsersRoute')
app.use('/Routes', UsersRoute)

const EmailRoute = require('./Routes/EmailRoute')
app.use('/Routes', EmailRoute)

const sk_test = require('./config.js')
const stripe  = require("stripe")(sk_test)
app.use('/Payment',require('./Routes/PaymentRoute'))
//PaymentRoute.js??


app.listen(3001, function(){
    console.log("Listening to port 3001")
})


// const app =require('express')();
// const mongoose = require('mongoose');

// // =============== body parser ===============
// const bodyParser= require('body-parser');
// app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());

// // =============== DATABASE CONNECTION =====================
// mongoose.connect('mongodb://127.0.0.1/movies_db',()=>{
//     console.log('*** connected to mongodb ***');
// });
// // =============== ROUTES ==============================
// const moviesRoute = require('./routes/MoviesRoute')
// // const todosRoute = require('./routes/todos');
// // const usersRoute = require('./routes/users');
// // =============== USE ROUTES ==============================
// app.use('/routes', moviesRoute)
// // app.use('/todos', todosRoute);
// // app.use('/users', usersRoute);
// // =============== START SERVER =====================
// app.listen(3000, () => 
//     console.log(`server listening on port 3000`
// ));


