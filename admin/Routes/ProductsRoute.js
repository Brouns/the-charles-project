const express = require('express');

const controller = require('../Controllers/ProductsController');
const router = express.Router();
// // Just added
// const mongo = require('mongodb'); 
// const assert = require('assert'); 

// var url = 'mongodb://localhost:3000'

router.get('/products', controller.findAll);

router.get('/products/:productName', controller.findOne);
 
router.post('/products/new', controller.insert);

router.post('/products/delete', controller.delete);

router.post('/products/deleteAll', controller.deleteAll);

router.post('/products/update', controller.update);

module.exports = router;

