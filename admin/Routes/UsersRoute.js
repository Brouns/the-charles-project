const express = require('express');

const controller = require('../Controllers/UsersController');
const router = express.Router();

router.get('/users', controller.findAll); //workes

router.get('/users/:emailInserted', controller.findOne); //workes
 
router.post('/users/new', controller.insert); //Workes but I can only add one person

router.post('/users/delete', controller.delete);//workes

router.post('/users/deleteAll', controller.deleteAll); //workes

router.post('/users/update', controller.update);//workes


module.exports = router;
