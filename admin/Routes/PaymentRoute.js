const router = require("express").Router();
const controller = require("../controllers/PaymentController");

router.post("/Charge", controller.charge);

module.exports = router;
