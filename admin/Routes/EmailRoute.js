const router     = require('express').Router();
const controller = require('../Controllers/EmailController.js')

router.post('/sendEmail',controller.send_email)

module.exports = router