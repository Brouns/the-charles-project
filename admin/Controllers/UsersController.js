
const Users = require('../Models/Users');

class UsersController {
    //FIND ALL
    async findAll(req, res){
            try{
                const allUsers = await Users.find({});
                res.send(allUsers);
            }
            catch(e){
                res.send({e})
            }
        }

        // FIND ONE TODO NAME
    async findOne(req ,res){
            let { emailInserted } = req.params;
            console.log( emailInserted )
            try{
                const userFind = await Users.findOne({email:emailInserted});
                res.send(userFind);
            }
            catch(e){
                res.send({e})
            }
    
        }

        // // POST ADD ONE //from Stefano
    async insert (req, res) {
            let { firstname } = req.body;
            let { lastname } = req.body; 
            let { email } = req.body; 
            let { password } = req.body; 
         

            console.log( firstname, lastname, password, email)

            try{
                const response = await Users.create({ firstname:firstname , lastname:lastname , email:email , password:password, administrator: false});
                res.send(response)
            }
            catch(e){
                res.send({e})
            }
        }



    //     // DELETE PRODUCT //
    async delete (req, res) {
            let { id } = req.body;
            console.log(id)
            try{
                const removed = await Users.remove({ _id:id});
                res.send({removed});   
            }
            catch(e){
                res.send({e})
            }
    
        }

    async deleteAll (req, res) {
            try{
                const response = await Users.remove({});
                res.send(response);   
            }
            catch(e){
                res.send({e})
            }
    
        }




    // UPDATE PRODUCT 
    //Now I have to put everything in the body,can't I change that? I a smart and simple way? 
        async update (req, res) {
            let { id } = req.body; 
            let { firstname } = req.body;
            let { lastname } = req.body; 
            let { email } = req.body; 
            let { password } = req.body; 
            let { administrator } = req.body; 
            
            try{
                const updated =  await Users.updateOne(
                    {_id:id}, {$set:{firstname:firstname, lastname:lastname, email:email, password:password, administrator:administrator}
                                }                );
                res.send({updated});
                }
             catch(e){
             res.send({e})
         }
    
        }

        
    }

module.exports = new UsersController();
    