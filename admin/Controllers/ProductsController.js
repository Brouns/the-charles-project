const Products = require('../Models/Products');

class ProductsController {
    //FIND ALL
    async findAll(req, res){
            try{
                const products = await Products.find({});
                res.send(products);
            }
            catch(e){
                res.send({e})
            }
        }

        // FIND ONE TODO NAME
    async findOne(req ,res){
            let { productName } = req.params;
            console.log( productName )
            try{
                const productFind = await Products.findOne({name:productName});
                res.send(productFind);
            }
            catch(e){
                res.send({e})
            }
    
        }

        


        
        // // POST ADD ONE //from Stefano
    async insert (req, res) {
            let { name } = req.body;
            let { fiveMin } = req.body; 
            let { tenMin } = req.body; 
            let { twentyMin } = req.body; 
            let { fiveMinStock } = req.body;
            let { tenMinStock } = req.body;
            let { twentyMinStock } = req.body;
            let { fiveMinSold } = req.body;
            let { tenMinSold} = req.body;
            let { twentyMinSold} = req.body;

            console.log(name, fiveMin, tenMin, twentyMin, fiveMinStock, tenMinStock, twentyMinStock, fiveMinSold, tenMinSold, twentyMinSold )
            try{
                const response = await Products.create({name:name, fiveMin:fiveMin, tenMin:tenMin, twentyMin:twentyMin, fiveMinStock:fiveMinStock, tenMinStock:tenMinStock, twentyMinStock:twentyMinStock, fiveMinSold:fiveMinSold, tenMinSold:tenMinSold, twentyMinSold:twentyMinSold});
                res.send(response)
            }
            catch(e){
                res.send({e})
            }
        }


//     async categorieInsert (req,res) {
//         let { movie } = req.body; 
//         let { categorie } = req.body;
//         console.log(categorie, movie)
//         try{
//             const updated =  await Movies.updateOne(
//                 {movie:movie}, {$set:{ categorie:categorie }}
//             );
//             res.send({updated});
//             }
//          catch(e){
//          res.send({e})
//     }
// }




    //     // DELETE PRODUCT
    async delete (req, res) {
            let { id } = req.body;
            console.log(id)
            try{
                const removed = await Products.remove({ _id:id});
                res.send({removed});   
            }
            catch(e){
                res.send({e})
            }
    
        }

    async deleteAll (req, res) {
            try{
                const response = await Products.remove({});
                res.send(response);   
            }
            catch(e){
                res.send({e})
            }
    
        }


    // UPDATE PRODUCT 
    //Now I have to put everything in the body,can't I change that? I a smart and simple way? 
        async update (req, res) {
            let { id } = req.body; 
            let { name } = req.body;
            let { fiveMin } = req.body; 
            let { tenMin } = req.body; 
            let { twentyMin } = req.body; 
            let { fiveMinStock } = req.body;  
            let { tenMinStock } = req.body;  
            let { twentyMinStock } = req.body; 
            let { fiveMinSold } = req.body;
            let { tenMinSold} = req.body;
            let { twentyMinSold} = req.body; 
            try{
                const updated =  await Products.updateOne(
                    {_id:id}, {$set:{name:name, fiveMin:fiveMin, tenMin:tenMin, twentyMin:twentyMin, fiveMinStock:fiveMinStock, tenMinStock:tenMinStock, twentyMinStock:twentyMinStock,  fiveMinSold:fiveMinSold, tenMinSold:tenMinSold, twentyMinSold:twentyMinSold}
                                }                );
                res.send({updated});
                }
             catch(e){
             res.send({e})
         }
    
        }
    }

module.exports = new ProductsController();
    

